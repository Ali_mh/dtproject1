package Test;

import Deposit.Deposit;
import FileIoUtility.ExportResult;
import FileIoUtility.XmlReader;

import java.util.Comparator;
import java.util.List;


public class Main {

    public static void main(String[] args) {

        List<Deposit> depositList = XmlReader.readFile("src/Test/Deposits.xml");
        depositList.sort(Comparator.comparing(Deposit::getPayedInterest).reversed());
        ExportResult.toTxtFile(depositList, "DepositsPayedInterest.txt");

    }
}
