package Deposit;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class LongTerm extends Deposit {


    @Override
    public void payedInterestCalculator() {
        if(interestRate==null)
        interestRate = 20f;
        this.payedInterest = depositBalance.multiply(BigDecimal.valueOf(durationInDays)).multiply(BigDecimal.valueOf(interestRate)).divide(BigDecimal.valueOf(YEARPERCENT), 0, RoundingMode.HALF_UP);

    }

}
