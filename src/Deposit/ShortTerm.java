package Deposit;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ShortTerm extends Deposit {

    @Override
    public void payedInterestCalculator() {
        if(interestRate==null)
        interestRate = 10f;
        this.payedInterest = depositBalance.multiply(BigDecimal.valueOf(durationInDays)).multiply(BigDecimal.valueOf(interestRate)).divide(BigDecimal.valueOf(YEARPERCENT), 0, RoundingMode.HALF_UP);

    }

}
