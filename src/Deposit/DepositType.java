package Deposit;

public enum DepositType {
    Qarz(0),
    ShortTerm(1),
    LongTerm(2),
    NotExistType(100);

    private final int depositTypeIndex;

    DepositType(int index) {
        this.depositTypeIndex = index;
    }

    public int getDepositTypeIndex() {
        return depositTypeIndex;
    }

    public static DepositType depositTypeValidation(String testDepositType) {
        try {
            return DepositType.valueOf(testDepositType);
        } catch (Exception e) {
        }

        return NotExistType;
    }
}
