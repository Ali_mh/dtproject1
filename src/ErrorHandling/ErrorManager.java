package ErrorHandling;

import Deposit.*;

import java.math.BigDecimal;

public class ErrorManager {
    public static boolean inputError(Deposit deposit) {
        int durationInDays = deposit.getDurationInDays();
        BigDecimal depositBalance = deposit.getDepositBalance();
        int customerNumber = deposit.getCustomerNumber();
        int depositType = deposit.getDepositType();
        try {
            if (durationInDays < 1)
                throw new Exception("DurationInDays cannot be negative or zero! Customer Number=" + customerNumber);
            if (depositBalance.signum() < 0)
                throw new Exception("DepositBalance cannot be negative! Customer Number=" + customerNumber);
            return false;
        } catch (Throwable throwable) {
            System.out.println(throwable.getMessage());
            return true;
        }
    }
}
