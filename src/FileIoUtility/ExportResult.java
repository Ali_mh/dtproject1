package FileIoUtility;

import Deposit.Deposit;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class ExportResult {
    public static void toTxtFile(List<Deposit> depositList, String pathName) {

        try {
            FileWriter fileWriter = new FileWriter(pathName);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (Deposit deposit : depositList) {
                String temp = String.valueOf(deposit.getCustomerNumber() + "#" + deposit.getPayedInterest());
                bufferedWriter.write(temp);
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
            System.out.println("File Saved.");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

