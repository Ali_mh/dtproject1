package FileIoUtility;
import Deposit.*;
import ErrorHandling.*;



import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class XmlReader {
    public static List<Deposit> readFile(String pathName) {
        List<Deposit> depositList = new ArrayList<Deposit>();

        try {
            File depositFile = new File(pathName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(depositFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("deposit");
            nodelistloop:
            for (Integer i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {

                    Element element = (Element) node;
                    Integer customerNumber;
                    BigDecimal depositBalance;
                    Integer durationInDays;
                    DepositType depositType;
                    String customerNumberString = element.getElementsByTagName("customerNumber").item(0).getTextContent();

                    try {
                        durationInDays = Integer.parseInt(element.getElementsByTagName("durationInDays").item(0).getTextContent());
                        depositBalance = (new BigDecimal(element.getElementsByTagName("depositBalance").item(0).getTextContent()));
                        customerNumber = Integer.parseInt(element.getElementsByTagName("customerNumber").item(0).getTextContent());
                        depositType = DepositType.depositTypeValidation(element.getElementsByTagName("depositType").item(0).getTextContent());
                    } catch (Exception e) {
                        System.out.println("Invalid input! CustomerNumber= " + customerNumberString);
                        continue;
                    }

                    Integer depositTypeIndex = depositType.getDepositTypeIndex();


                    Deposit deposit = new Deposit();
                    switch (depositType) {
                        case Qarz:
                            deposit = Qarz.class.newInstance();
                            break;
                        case ShortTerm:
                            deposit = ShortTerm.class.newInstance();
                            break;
                        case LongTerm:
                            deposit = LongTerm.class.newInstance();
                            break;
                        case NotExistType:System.out.println("DepositType does not exist! Customer Number="+customerNumberString); continue nodelistloop;
                    }

                    Method setCustomerNumber = deposit.getClass().getSuperclass().getDeclaredMethod("setCustomerNumber", Integer.class);
                    Method setDepositBalance = deposit.getClass().getSuperclass().getDeclaredMethod("setDepositBalance", BigDecimal.class);
                    Method setDurationInDays = deposit.getClass().getSuperclass().getDeclaredMethod("setDurationInDays", Integer.class);
                    Method setDepositType = deposit.getClass().getSuperclass().getDeclaredMethod("setDepositType", Integer.class);

                    setCustomerNumber.invoke(deposit, customerNumber);
                    setDepositBalance.invoke(deposit, depositBalance);
                    setDurationInDays.invoke(deposit, durationInDays);
                    setDepositType.invoke(deposit, depositTypeIndex);


                    if (ErrorManager.inputError(deposit))
                        continue;

                    deposit.payedInterestCalculator();
                    depositList.add(deposit);


                }
            }

            System.out.println("File read successfully.");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return depositList;
    }
}
